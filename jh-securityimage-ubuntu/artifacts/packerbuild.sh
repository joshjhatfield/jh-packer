#!/bin/bash
# Packer build script, places conf files and runs commands.
# Run as last provisioner.


run(){
	# 1. Shared mem

	sudo cat /tmp/packer/01_sharedmem_etcfstab >> /etc/fstab

	#2. sshd config

	sudo cat /tmp/packer/02_sshd_config > /etc/ssh/sshd_config

	#3. Pam settings
	sudo cat /tmp/packer/03_pamlogin_cracklib > /etc/pam.d/login

	#4. crontab add

	sudo cat /tmp/packer/04_crontab > /var/spool/cron/crontabs/root
	sudo chown root:crontab /var/spool/cron/crontabs/root
	sudo chmod 700 /var/spool/cron/crontabs/root
}

case $1 in
	run)
	run
		;;
esac



